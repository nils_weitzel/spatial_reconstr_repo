#Wrapper for all reconstructions
reconstruction <- function(reconstr_meta,mcmc_meta,reconstr_directories) {
  #Initialization
  cat("Start preparation of MCMC \n")
  source("Source/Help_Functions.R"); packages()
  title=reconstr_meta$title;clim_var=reconstr_meta$clim_var;lon=reconstr_meta$lon;lat=reconstr_meta$lat;process_stage_model=reconstr_meta$process_stage_model;cov_matrix=reconstr_meta$cov_matrix;models=reconstr_meta$models;tf=reconstr_meta$tf;tf_params_type=reconstr_meta$tf_params_type
  nr_samples=mcmc_meta$nr_samples;burnin=mcmc_meta$burnin;thin=mcmc_meta$thin
  simulation_file=reconstr_directories$simulation_file;covariance_file=reconstr_directories$covariance_file;shrinkage_param_file=reconstr_directories$shrinkage_param_file;proxy_file=reconstr_directories$proxy_file;tf_param_file=reconstr_directories$tf_param_file
  num_clim_var <- length(clim_var)
  
  #Prepare for MC3 when KM is used
  if  (process_stage_model == "km") {
    mc3_params <- set_mc3(nr_samples,burnin,thin)
    it <- mc3_params$it; loops <- mc3_params$loops; base <- mc3_params$base; num_chains <- mc3_params$num_chains; min_exp <- mc3_params$min_exp; tempering_values <- mc3_params$tempering_values
    total_samples <- it * loops
  } else {
    total_samples <- it <- burnin + nr_samples * thin
  }
  
  #Preparation of process stage
  ensemble_data <- list()
  load(simulation_file)
  if (cov_matrix == "shrinkage") {
    load(shrinkage_param_file)
    #Compute shrinkage covariance matrix from empirical covariance and shrinkage parameters
    tmp <- compute_shrinkage(clim_var,simulation_data$climatologies,shrinkage_params)
    ensemble_data$cov <- tmp$cov; ensemble_data$prec <- tmp$prec
  }
  if (cov_matrix == "glasso") {
    load(covariance_file)
    ensemble_data$prec <- cov_data$prec
    ensemble_data$cov <- cov_data$cov
  }
  if (process_stage_model == "gm") {
    #Gaussian model
    ensemble_data$means <- matrix(apply(simulation_data$climatologies,2,mean),nrow=1)
  } else { 
    ensemble_data$means <- simulation_data$climatologies
  }
  num_points <- dim(simulation_data$climatologies)[2]; num_models <- dim(simulation_data$climatologies)[1]; num_gridpoints <- num_points / num_clim_var
  if (process_stage_model == "km") {
    #Calculate Silverman factor
    sf <- (4/(num_models*(num_points+2)))^(2/(num_points+4))
    #Adjust covariance matrix by Silverman factor
    ensemble_data$cov <- sf * ensemble_data$cov
    ensemble_data$prec <- sf * ensemble_data$prec
  }
  
  #Preparation of proxy data
  load(proxy_file)
  proxy_loc_nr <- proxy_data$loc_nr
  proxy_values <- proxy_data$values
  if (tf == "Gauss" | tf == "Gauss_biv") {
    proxy_cov <- proxy_data$cov
    #Sort by proxy_loc_nr <- list with vector of values and matrix of covariances for each proxy_loc_nr
  }
  if (tf == "PITM") {
    taxanames <- proxy_data$taxanames
    num_taxa <- length(taxanames)
    if (tf_params_type == "full") {
      #Calibration data for transfer functions
      #LOAD MODERN DATA (NOT INCLUDED IN THIS REPOSITORY)
      #modern_data is list with lists of Clim_values and kappa at each calibration point for each taxa
      #kappa = presence/absene - 0.5
      #Change Taxadata for TF parameter estimation (add values for each proxy sample)
      library(BayesLogit)
      tf_params <- list()
      num_modern_data <- 0
      for (k in 1:num_taxa) {
        num_modern_data[k] <- length(modern_data$Clim_values[[k]][,1])
        clim_tmp <- modern_data$Clim_values[[k]]
        modern_data$Clim_values[[k]] <- array(0,dim=c(num_modern_data[k]+length(proxy_loc_nr),num_clim_var))
        modern_data$Clim_values[[k]][1:num_modern_data[k],] <- clim_tmp
        modern_data$kappa[[k]] <- c(modern_data$kappa[[k]],proxy_values[,k]-0.5)
      }
    } else {
      load(tf_param_file)
      modern_data <- list(); num_modern_data <- 0
    }
  }

  #Reduction to data points, update ensemble_data model and proxy structure
  #With this, MCMC is run only for grid boxes with proxy samples, and the remaining boxes are filled afterwards
  ensemble_data_alt <- ensemble_data
  proxy_loc_nr_alt <- proxy_loc_nr
  if (tf=="PITM" | tf == "Gauss_biv") {
    proxy_loc_nr_uni <- rep((0:(num_clim_var-1)*num_gridpoints),each=length(unique(proxy_loc_nr_alt))) + rep(sort(unique(proxy_loc_nr_alt)),times=num_clim_var)
  } else {
    proxy_loc_nr_uni <- sort(unique(proxy_loc_nr))
  }
  noproxy_loc_nr <- which(1:num_points %notin% proxy_loc_nr_uni)
  if (cov_matrix =="shrinkage") {
    ensemble_data$prec_nodata <- ensemble_data$cov_nodata <- array(0,dim=c(num_models,length(noproxy_loc_nr),length(noproxy_loc_nr)))
    ensemble_data$cov <- ensemble_data$cov[,proxy_loc_nr_uni,proxy_loc_nr_uni]
    ensemble_data$prec_nodata <- ensemble_data_alt$prec[,noproxy_loc_nr,noproxy_loc_nr]
    ensemble_data$prec <- array(0,dim=c(num_models,length(proxy_loc_nr_uni),length(proxy_loc_nr_uni)))
    for (i in 1:num_models) {
      ensemble_data$prec[i,,] <- solve(ensemble_data$cov[i,,])
      ensemble_data$cov_nodata[i,,] <- solve(ensemble_data$prec_nodata[i,,])
    }
  } else {
    ensemble_data$cov <- ensemble_data$cov[proxy_loc_nr_uni,proxy_loc_nr_uni]
    ensemble_data$prec <- solve(ensemble_data$cov)
    ensemble_data$prec_nodata <- ensemble_data_alt$prec[noproxy_loc_nr,noproxy_loc_nr]
    ensemble_data$cov_nodata <- solve(ensemble_data$prec_nodata)
  }
  ensemble_data$means <- matrix(ensemble_data$means[,proxy_loc_nr_uni],ncol=length(proxy_loc_nr_uni))
  ensemble_data$means_nodata <- matrix(ensemble_data_alt$means[,noproxy_loc_nr],ncol=length(noproxy_loc_nr))
  for (i in 1:length(proxy_loc_nr_alt)) {
    proxy_loc_nr[i] <- which(proxy_loc_nr_alt[i] == proxy_loc_nr_uni)
  }
  num_points_alt <- num_points
  num_points <- length(proxy_loc_nr_uni)
  num_gridpoints_alt <- num_gridpoints
  num_gridpoints <- length(unique(proxy_loc_nr))
  
  #PREPARATION OF MCMC CHAIN
  #Define parameters of prior distributions
  prior <- define_prior_distributions(process_stage_model,tf_params_type,num_models,num_clim_var)
  #Define starting values of Markov chains
  if (tf == "PITM") {
    starting_values <- define_starting_values(ensemble_data$means,tf,num_models,prior$alpha,num_clim_var,num_taxa)
  } else {
    starting_values <- define_starting_values(ensemble_data$means,tf,num_models,prior$alpha)
  }
  
  #RUN MCMC / MC3
  source("Source/MCMC_Functions.R")
  if (process_stage_model == "km") {
    cat("Start Metropolis coupled MCMC \n")
    if (tf == "PITM") {
      posterior <- reconstr_mc3(total_samples,loops,it,num_chains,tempering_values,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_data$means,ensemble_data$prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,tf_params_type,num_taxa,tf_params,modern_data,num_modern_data)
    } else {
      posterior <- reconstr_mc3(total_samples,loops,it,num_chains,tempering_values,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_data$means,ensemble_data$prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,proxy_cov=proxy_cov)
    }
    cat("Metropolist coupled MCMC completed \n")
  } else {
    cat("Start MCMC \n")
    if (tf == "PITM") {
      posterior <- reconstr_mcmc(it,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_data$means,ensemble_data$prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,tf_params_type,num_taxa,tf_params,modern_data=modern_data,num_modern_data=num_modern_data,tempering=1)
    } else {
      posterior <- reconstr_mcmc(it,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_data$means,ensemble_data$prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,tempering=1,proxy_cov=proxy_cov)
    }
    cat("MCMC completed \n")
  }
  
  #THINNING OF MCMC OUTPUT
  if (tf == "PITM") {
    posterior$reg <- thinning(posterior$reg,total_samples,burnin,thin)
  }
  if (process_stage_model == "rm") {
    posterior$omega <- thinning(posterior$omega,total_samples,burnin,thin)
  }
  if (process_stage_model == "km") {
    posterior$omega <- thinning(posterior$omega,total_samples,burnin,thin)
    posterior$z <- thinning(posterior$z,total_samples,burnin,thin)
  }
  if (cov_matrix == "shrinkage") {
    posterior$prec_model <- thinning(posterior$prec_model,total_samples,burnin,thin)
  }
  posterior$clim <- thinning(posterior$clim,total_samples,burnin,thin)
  posterior$acceptances <- thinning(posterior$acceptances,total_samples,burnin,thin)
  posterior$times <- thinning(posterior$times,total_samples,burnin,thin)
  cat("Thinning of MCMC samples complete \n")
  
  #ADD SAMPLES FOR GRID BOXES WITHOUT PROXY DATA AND COME BACK TO ORIGNIAL STRUCTURE
  cat("Start sampling for grid boxes without proxy data \n")
  t1 <- proc.time()[3]
  #Restructure lists
  posterior_sep <- posterior
  posterior$clim <- array(0,dim=c(nr_samples,num_points_alt))
  posterior$clim[,proxy_loc_nr_uni] <- posterior_sep$clim
  indices <- which(1:num_points_alt %notin% proxy_loc_nr_uni)
  indices_proxies <- proxy_loc_nr_uni
  precision_matrix <- ensemble_data_alt$prec
  cond_precision <- ensemble_data$prec_nodata
  cond_covariance <- ensemble_data$cov_nodata
  #Create samples from covariance matrix
  if (cov_matrix == "shrinkage") {
    nodata_values <- array(0,dim=c(nr_samples,length(indices)))
    for (i in 1:num_models) {
      nodata_values[which(posterior$prec_model == i),] <- rmvnorm.prec(length(which(posterior$prec_model == i)),rep(0,times=length(indices)),Q=cond_precision[i,,])
    }
  } else {
    nodata_values <- rmvnorm.prec(nr_samples,rep(0,times=length(indices)),Q=cond_precision)
  }
  #Add conditional mean given samples from points with data to samples from covariance matrix
  means <- apply(ensemble_data_alt$means,2,mean)
  for (i in 1:(nr_samples)) {
    #Unconditioned mean given process stage parameters
    if (process_stage_model == "rm") {
      means <- as.numeric(posterior$omega[i,] %*% ensemble_data_alt$means)
    }
    if (process_stage_model == "km") {
      means <- ensemble_data_alt$means[which(posterior$z[i,]==1),]
    }
    #Values that are already estimated
    new_sample <- c(posterior$clim[i,])
    #Calculate conditional mean
    if (cov_matrix == "shrinkage") {
      cond_mean <- means[indices] - as.numeric(solve(cond_precision[posterior$prec_model[i],,],precision_matrix[posterior$prec_model[i],indices,indices_proxies] %*% (new_sample[indices_proxies] - means[indices_proxies])))
    } else {
      cond_mean <- means[indices] - as.numeric(solve(cond_precision,precision_matrix[indices,indices_proxies] %*% (new_sample[indices_proxies] - means[indices_proxies])))
    }
    #Add conditional mean to previously created samples
    new_sample[indices] <- cond_mean + nodata_values[i,]
    posterior$clim[i,] <- new_sample
  }  
  t2 <- proc.time()[3]
  cat("Samples for grid boxes without proxy data drawn; Time:", (t2-t1)/60, " min \n")

  #MCMC DIAGNOSTICS
  cat("Acceptance rate of Metropolis Hastings steps: ",round(mean(posterior$acceptances),3),"\n",sep="")
  #END
  return(posterior)
}

#Wrapper for Metropolis within Gibbs MCMC (Update of TF params, process stage params, and climate with single Markov chain)
reconstr_mcmc <- function(it,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_means,ensemble_prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models=1,tf_params_type="constant",num_taxa="1",tf_params=list(),modern_data=list(),num_modern_data=0,tempering=1,proxy_cov=1) {
  #Initializing of reconstruction
  posterior <- list()
  num_clim_var <- length(clim_var); num_gridpoints <- num_points / num_clim_var
  if (tf == "PITM" & tf_params_type == "full") {
    posterior$reg <- array(0,dim=c(1+it,3*num_clim_var*num_taxa)); posterior$reg[1,] <- starting_values$reg
  }
  if (process_stage_model == "rm") {
    posterior$omega <- array(0,dim=c(1+it,num_models)); posterior$omega[1,] <- starting_values$omega
  }
  if (process_stage_model == "km") {
    posterior$omega <- array(0,dim=c(1+it,num_models)); posterior$omega[1,] <- starting_values$omega
    posterior$z <- array(0,dim=c(1+it,num_models)); posterior$z[1,] <- starting_values$z
  }
  if (cov_matrix == "shrinkage") {
    posterior$prec_model <- array(0,dim=c(1+it)); posterior$prec_model[1] <- starting_values$prec_model
  }
  posterior$clim <- array(0,dim=c(1+it,num_points)); posterior$clim[1,] <- starting_values$clim
  posterior$acceptances <- array(0,dim=c(1+it,num_gridpoints))
  posterior$times <- array(0,dim=c(1+it))
  #Correct precision matrix with tempering value
  if (cov_matrix == "shrinkage") {
    precision_matrix <- tempering * ensemble_prec[starting_values$prec_model,,]
  } else {
    precision_matrix <- tempering * ensemble_prec
  }
  mean_tmp <- apply(ensemble_means,2,mean)
  #MCMC
  t0 <- proc.time()[3]
  for( i in 2:(it+1) ) {
    t1 <- proc.time()[3]
    #Update TF parameters (regression coefficients)
    if (tf == "PITM") {
      if (tf_params_type == "full") {
        for (k in 1:num_taxa) {
          for(j in 1:num_clim_var) {
            modern_data$Clim_values[[k]][num_modern_data[k]+(1:length(proxy_loc_nr)),j] <- posterior$clim[i-1,(j-1)*num_gridpoints+proxy_loc_nr]-273.15
          }
          modern_data$kappa[[k]] <- c(modern_data$kappa[[k]][1:num_modern_data[k]],tempering*(proxy_values[,k]-0.5))
        }
        posterior$reg[i,] <- update_regcoeff(num_taxa,num_clim_var,posterior$reg[i-1,],modern_data,prior$beta_prec,prior$beta_mean)
        taxa_params <- posterior$reg[i,]
      } else {
        if (tf_params_type == "modularized") {
          taxa_params <- tf_params$params[i-1,]
        } else {
          taxa_params <- tf_params$params
        }
      }
    }
    if (process_stage_model == "rm") {
      #Update omega (with random walk Metropolis-Hastings)
      posterior$omega[i,] <- update_omega_regr(prior$alpha, prior$phi, posterior$omega[i-1,], posterior$clim[i-1,], ensemble_means, precision_matrix)$omega
      mean_tmp <- as.numeric(posterior$omega[i,] %*% ensemble_means)
    }
    if (process_stage_model == "km") {
      #Update omega (with Gibbs)
      posterior$omega[i,] <- update_omega(prior$alpha, prior$phi, posterior$z[i-1,])
      #Update z (with Gibbs)
      posterior$z[i,] <- update_z(num_models,posterior$omega[i,],posterior$clim[i-1,],ensemble_means,precision_matrix)
      mean_tmp <- ensemble_means[which(posterior$z[i,]==1),]
    }
    #Update prec model
    if (cov_matrix == "shrinkage") {
      posterior$prec_model[i] <- update_shrinkage(num_models,posterior$prec_model[i-1],num_points,num_gridpoints,posterior$clim[i-1,],mean_tmp,tempering * ensemble_prec)$prec_model
      precision_matrix <- tempering * ensemble_prec[posterior$prec_model[i],,]
    }
    #Update climate (with RW MH)
    if (tf == "PITM") {
      temp <- update_clim(posterior$clim[i-1,],num_clim_var,num_gridpoints,mean_tmp,precision_matrix,tf,proxy_loc_nr,proxy_values,tempering,taxa_params)
    } else {
      temp <- update_clim(posterior$clim[i-1,],num_clim_var,num_gridpoints,mean_tmp,precision_matrix,tf,proxy_loc_nr,proxy_values,tempering,proxy_cov=proxy_cov)
    }
    posterior$clim[i,] <- temp[1:(num_points)]
    posterior$acceptances[i,] <- temp[(num_points+1):length(temp)]
    #Iteration analytics
    t2 <- proc.time()[3]
    posterior$times[i] <- t2-t1
    if (i %% 3000 == 0) {
      cat("Iteration",i,"completed, Total time:",round((proc.time()[3]-t0)/60,2),"min \n")
    }
  }
  return(posterior)
}

#Short version for MCMC updates in MC3; All parameters besides starting_values and tempering fixed
reconstr_mcmc_pitm <- function(starting_values,tempering) {
  return(reconstr_mcmc(it,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_means,ensemble_prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,tf_params_type,num_taxa,tf_params,modern_data,num_modern_data,tempering))
}

reconstr_mcmc_gauss <- function(starting_values,tempering) {
  return(reconstr_mcmc(it,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_means,ensemble_prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models,tempering=tempering,proxy_cov=proxy_cov))
}

#Wrapper for Metropolis coupled MCMC (Preparation; Update of TF params, process stage params, and climate in parallel; Metropolis coupling of chains [Swap chains])
reconstr_mc3 <- function(total_samples,loops,it,num_chains,tempering_values,clim_var,num_points,process_stage_model,cov_matrix,tf,ensemble_means,ensemble_prec,proxy_values,proxy_loc_nr,prior,starting_values,num_models=1,tf_params_type="constant",num_taxa="1",tf_params=list(),modern_data=list(),num_modern_data=0,proxy_cov=1) {
  num_clim_var <- length(clim_var); num_gridpoints <- num_points / num_clim_var
  swaps <- array(0,dim=c(loops,num_chains-1))
  chains_saved <- c(1)
  #Initializing Markov chains
  real_chain <- t(array(1:length(tempering_values),dim=c(length(tempering_values),loops)))
  posterior <- list()
  if (tf == "PITM" & tf_params_type == "full") {
    posterior$reg <- array(0,dim=c(1+total_samples,3*num_clim_var*num_taxa)); posterior$reg[1,] <- starting_values$reg
  }
  posterior$omega <- array(0,dim=c(1+total_samples,num_models)); posterior$omega[1,] <- starting_values$omega
  posterior$z <- array(0,dim=c(1+total_samples,num_models)); posterior$z[1,] <- starting_values$z
  if (cov_matrix == "shrinkage") {
    posterior$prec_model <- array(0,dim=c(1+total_samples)); posterior$prec_model[1] <- starting_values$prec_model
  }
  posterior$clim <- array(0,dim=c(1+total_samples,num_points)); posterior$clim[1,] <- starting_values$clim
  posterior$acceptances <- array(0,dim=c(1+total_samples,num_gridpoints))
  posterior$times <- array(0,dim=c(1+total_samples))
  
  #Initializing parallel chains
  t1 <- proc.time()[3]
  cl <- makeCluster(num_chains,type="PSOCK")
  clusterExport(cl, c("it","clim_var","num_points","process_stage_model","cov_matrix","tf","ensemble_means","ensemble_prec","prior","num_models"),envir = environment())
  clusterExport(cl, c("update_omega","update_z","update_shrinkage","update_clim","reconstr_mcmc","%notin%"),envir = environment())
  clusterEvalQ(cl, c(library(MCMCpack),library(spam),library(Matrix)))
  if (tf == "PITM") {
    #tf_params_type,num_taxa,modern_data
    clusterExport(cl, c("tf_params_type","num_taxa","proxy_values","proxy_loc_nr","likelihood_ratio"),envir = environment())
    if (tf_params_type == "full") {
      clusterExport(cl, c("modern_data","num_modern_data","update_regcoeff","update_beta"),envir = environment())
      clusterEvalQ(cl, c(library(BayesLogit)))
    }
    if (tf_params_type == "modularized") {
      tf_params_all <- tf_params
      tf_params <- list(taxanames = tf_params_all$taxanames)
    } else {
      clusterExport(cl,"tf_params",envir = environment())
    }
  } else {
    clusterExport(cl, c("proxy_values","proxy_cov","proxy_loc_nr"),envir = environment())
  }
  posterior <- rep(list(posterior),times=length(chains_saved))
  starting_values <- rep(list(starting_values),times=length(tempering_values))

  #Run parallel chains
  t0 <- proc.time()[3]
  for (i in 1:loops) {
    if (tf == "PITM" & tf_params_type == "modularized") {
        tf_params$params <- tf_params_all$params[(it*(i-1))+1:it,]
        clusterExport(cl,"tf_params",envir = environment())
    }
    #Run each chain for fixed number of it
    if (tf == "PITM") {
      posterior_temp <- clusterMap(cl,reconstr_mcmc_pitm,starting_values,tempering_values)
    } else {
      posterior_temp <- clusterMap(cl,reconstr_mcmc_gauss,starting_values,tempering_values)
    }
    #Add new iterations to reconstruction list
    for (j in 1:length(chains_saved)) {
      temp_index <- which(real_chain[i,] == chains_saved[j])
      if (tf == "PITM" & tf_params_type == "full") {
        posterior[[j]]$reg[(it*(i-1))+2:(it+1),] <- posterior_temp[[temp_index]]$reg[2:(it+1),]
      }
      posterior[[j]]$omega[(it*(i-1))+2:(it+1),] <- posterior_temp[[temp_index]]$omega[2:(it+1),]
      posterior[[j]]$z[(it*(i-1))+2:(it+1),] <- posterior_temp[[temp_index]]$z[2:(it+1),]
      if (cov_matrix == "shrinkage") {
        posterior[[j]]$prec_model[(it*(i-1))+2:(it+1)] <- posterior_temp[[temp_index]]$prec_model[2:(it+1)]
      }
      posterior[[j]]$clim[(it*(i-1))+2:(it+1),] <- posterior_temp[[temp_index]]$clim[2:(it+1),]
      posterior[[j]]$acceptances[(it*(i-1))+2:(it+1),] <- posterior_temp[[temp_index]]$acceptances[2:(it+1),]
      posterior[[j]]$times[(it*(i-1))+2:(it+1)] <- posterior_temp[[temp_index]]$times[2:(it+1)]
    }
    for (j in 1:length(tempering_values)) {
      starting_values[[j]] <- list( omega=posterior_temp[[j]]$omega[it+1,], z=posterior_temp[[j]]$z[it+1,], clim=posterior_temp[[j]]$clim[it+1,] )
      if (tf == "PITM") {
        starting_values[[j]]$reg <- posterior_temp[[j]]$reg[it+1,]
      }
      if (cov_matrix == "shrinkage") {
        starting_values[[j]]$prec_model=posterior_temp[[j]]$prec_model[it+1]
      }
    }
    #Perform swaps
    if (i < loops) {
      real_chain[i+1,] <- real_chain[i,]
      for (k in 1:(length(tempering_values)-1)) {
        j <- length(tempering_values) - k
        temp_index1 <- which(real_chain[i+1,] == j)
        temp_index2 <- which(real_chain[i+1,] == (j+1))
        if (tf == "PITM") {
          if (tf_params_type == "modularized") {
            tf_params$params <- tf_params$params[it,]
          }
          acc <- swap_states(temp_index1,temp_index2,tempering_values,num_clim_var,ensemble_means,ensemble_prec,cov_matrix,starting_values,tf,num_gridpoints,proxy_loc_nr,proxy_values,tf_params,tf_params_type)
        }
        if (tf == "Gauss" | tf == "Gauss_biv") {
          acc <- swap_states(temp_index1,temp_index2,tempering_values,num_clim_var,ensemble_means,ensemble_prec,cov_matrix,starting_values,tf,num_gridpoints,proxy_loc_nr,proxy_values,proxy_cov=proxy_cov)
        }
        if (acc == 1) {
          tempering_values[c(temp_index1,temp_index2)] <- tempering_values[c(temp_index2,temp_index1)]
          real_chain[i+1,c(temp_index1,temp_index2)] <- real_chain[i+1,c(temp_index2,temp_index1)]
          swaps[i,j] <- 1
        }
      }
    }
    if (i %% 100 == 0) {
      cat("Iteration",i*it,"of MC3 completed, Total time:",round((proc.time()[3]-t0)/60,2),"min \n")
      
    }
    # if (tf_params == "full" & ((i %% 100) == 0)) {
    #   save(i,posterior,starting_values,real_chain,tempering_values,swaps,file=paste(title,".RData",sep=""))
    # }
  }
  
  stopCluster(cl)
  rm(cl)
  t2 <- proc.time()[3]
  cat("Time of MC^3 (using only gridpoints with proxy data):", (t2-t1)/60, " min \n")
  
  #Restructure posterior
  posterior <- posterior[[1]]
  posterior$swaps <- swaps

  #MCMC diagnostics:
  cat("Acceptance rate of Metropolis Hastings steps: ",round(mean(posterior$acceptances),3),"\n",sep="")
  cat("Fraction of swaps of MCMC chains (from i-th to (i+1)-th chain:",round(apply(swaps,2,mean),3),"\n",sep=" ")

  return(posterior)
}

